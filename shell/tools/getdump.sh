#!/usr/bin/env bash
# Be sure to have SSH "extn" alias
date
echo "Start getting dump!"
DBUSER=existyle
DBNAME=shop_exitenn
CUR_DATE=$(date +%Y-%m-%d---%H-%M)
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
DR="$(pwd)"
cd $THIS_DIR
BKP_DIR=$DR/var/backups
BKP_FILE=$DBNAME.local-dump.$CUR_DATE.sql
DBFILE=$DBNAME.sql
FILENAME=$DBFILE.7z
DESTINATION=$BKP_DIR/$DBFILE
DESTINATION_ZIP=$BKP_DIR/$FILENAME
DESTINATION_BKP=$BKP_DIR/$BKP_FILE

if [ -f $DESTINATION_ZIP ]
then
	echo "Rename (backup dump) previous archive file"
	mv $DESTINATION_ZIP $DESTINATION_ZIP.$CUR_DATE
fi

if [ -f $DESTINATION ]
then
	echo "Backup previous SQL file"
	mv -f $DESTINATION $DESTINATION.$CUR_DATE.sql
fi

echo "Start backup remote database: $DBNAME"
ssh extn "rm -f $FILENAME;mysqldump --defaults-extra-file=mysql.cnf --opt $DBNAME | 7z a -si $FILENAME"
rsync -upago extn:~/$FILENAME $DESTINATION_ZIP

if [ -f $DESTINATION_ZIP ]
then
    cd $BKP_DIR
    echo "Start unzip: $DESTINATION_ZIP"
    7z e $DESTINATION_ZIP
    echo "Make local database backup"
    mysqldump --defaults-extra-file=$THIS_DIR/mysql.cnf --opt $DBNAME > $DESTINATION_BKP
    if [ -f $DESTINATION ]; then
        echo "Run DB restore process"
	    mysql --defaults-extra-file=$THIS_DIR/mysql.cnf $DBNAME < $DESTINATION
    else
	    echo "There is NO remote dump file: $DESTINATION"
	    exit 1
	fi
	echo "Run DB updates"
	mysql --defaults-extra-file=$THIS_DIR/mysql.cnf $DBNAME < $BKP_DIR/remote.dump.helper.sql
	mv -f $DESTINATION $DESTINATION.prod.$CUR_DATE.sql
else
    echo "File has not downloaded: $DESTINATION_ZIP"
fi

bash $THIS_DIR/rmcache.sh
date
