#!/usr/bin/env bash
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd "../.."
DR="$(pwd)"
CUR_DATE=$(date +%Y-%m-%d---%H-%I)

AS_ROOT=0
if [[ $EUID -eq 0 ]]; then
   echo "This script has been run under root user "$EUID
   AS_ROOT=1
fi

echo "Clean folders:
var/cache/*"
rm -rf $DR/var/cache/*

echo "media/tmp/catalog/product/* "
rm -rf $DR/media/tmp/catalog/product/*

echo "media/css/*.css"
rm -rf $DR/media/css/*.css

echo "media/js/*.js"
rm -rf $DR/media/js/*.js

echo "Delete all old session files"
find $DR/var/session/ -type f -mtime +1 -exec rm -f {} \;

if [ -f $DR/var/log/exception.log -a -s $DR/var/log/exception.log ]
then
    echo "Clean exception.log"
    cp $DR/var/log/exception.log $DR/var/log/exception.$CUR_DATE.log
    cat /dev/null > $DR/var/log/exception.log
else
	echo "No exception.log OR file size = 0"
fi

if [ -f $DR/var/log/system.log -a -s $DR/var/log/system.log ]
then
    echo "Clean system.log"
    cp $DR/var/log/system.log $DR/var/log/system.$CUR_DATE.log
    cat /dev/null > $DR/var/log/system.log
else
	echo "No system.log OR file size = 0"
fi

if [[ $AS_ROOT -eq 1 ]];
then
    echo "Restart Web services"
    if [ -d /var/lib/nginx_pagespeed ]
    then
        echo "Remove PageSpeed Cache"
        rm -rf /var/lib/nginx_pagespeed/*
    fi
    /etc/init.d/nginx restart
    /etc/init.d/php5-fpm restart
#    /etc/init.d/mysql restart
fi
