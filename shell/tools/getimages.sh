#!/usr/bin/env bash
date
echo "Start getting images!"
SOURCE="${BASH_SOURCE[0]}"
THIS_DIR="$(cd -P "$( dirname "$SOURCE" )" && pwd)"
cd $THIS_DIR
cd '../..'
KB_DIR="$(pwd)"

cd $KB_DIR/media/catalog/category
rsync -uvar --exclude=cache --size-only kb:~/www/htroot/media/catalog/category/ .

cd $KB_DIR/media/catalog/product
rsync -uvar --exclude=cache --size-only kb:~/www/htroot/media/catalog/product/ .

bash $KB_DIR/rmcache.sh
date
