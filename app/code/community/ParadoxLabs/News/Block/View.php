<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_News Magento Plugin
 *	Create and edit simple news entries via the Admin Panel to be displayed on the front-end.
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_News_Block_View extends Mage_Core_Block_Template
	{
		public function _prepateLayout() {
			return parent::_prepareLayout();
		}
		
		public function getNewsTitles($offset=2,$num=3) {
			$w = Mage::getSingleton('core/resource')->getConnection('core_read');
			$table = Mage::getSingleton('core/resource')->getTableName('news/news');
			$store = Mage::app()->getStore()->getStoreId();
			
			$result = $w->select()
						->from($table)
						->where('news_store in(0,?)', $store )
						->order('news_id desc')
						->limit($num, $offset);
			
			return $w->fetchAll($result);
		}
		
		public function getLatestNews($num=2) {
			$w = Mage::getSingleton('core/resource')->getConnection('core_read');
			$table = Mage::getSingleton('core/resource')->getTableName('news/news');
			$store = Mage::app()->getStore()->getStoreId();
			
			$result = $w->select()
						->from( array( 'n' => $table),
								array(	'news_id',
										'news_date',
										'news_title',
										'news_text' => "concat(substring_index(n.news_text,' ',75),'...')" ) )
						->where('news_store in(0,?)', $store )
						->order('news_id desc')
						->limit($num, 0);
			
			return $w->fetchAll($result);
		}
	}
