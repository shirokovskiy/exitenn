<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_News Magento Plugin
 *	Create and edit simple news entries via the Admin Panel to be displayed on the front-end.
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_News_Block_Adminhtml_News_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->_objectId = 'id';
			$this->_blockGroup = 'news';
			$this->_controller = 'adminhtml_news';
			
			$this->_updateButton('save', 'label', Mage::helper('news')->__('Save Entry'));
			$this->_updateButton('delete', 'label', Mage::helper('news')->__('Delete Entry'));

			if( $this->getRequest()->getParam($this->_objectId) ) {
				$model = Mage::getModel('news/news')->load($this->getRequest()->getParam($this->_objectId));
				Mage::register('news', $model);
			}
		}

		public function getHeaderText()
		{
			if( Mage::registry('news') && Mage::registry('news')->getId() ) {
				return Mage::helper('news')->__('Edit Entry');
			} else {
				return Mage::helper('news')->__('Add Entry');
			}
		}
	}