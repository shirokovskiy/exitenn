<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_News Magento Plugin
 *	Create and edit simple news entries via the Admin Panel to be displayed on the front-end.
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_News_Block_Adminhtml_News_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
	{
		protected function _prepareLayout()
		{
			parent::_prepareLayout();
			if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
				$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			}
		}
		
		protected function _prepareForm()
		{
			$form = new Varien_Data_Form(array(
				'id' => 'edit_form',
				'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
				'method' => 'post'
			));

			$fieldset = $form->addFieldset('news_form', array(
				'legend'	=> Mage::helper('news')->__('News'),
				'class'		=> 'fieldset-wide'
			));

			$fieldset->addField('news_title', 'text', array(
				'name'      => 'news_title',
				'label'     => Mage::helper('news')->__('Title'),
				'title'     => Mage::helper('news')->__('Title'),
				'required'  => true,
			));

			$fieldset->addField('news_store', 'select', array(
				'name'      => 'news_store',
				'label'     => Mage::helper('core')->__('Store View'),
				'title'     => Mage::helper('core')->__('Store View'),
				'required'  => true,
				'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
			));

			$fieldset->addField('news_text', 'editor', array(
				'name'      => 'news_text',
				'label'     => Mage::helper('news')->__('Text'),
				'title'     => Mage::helper('news')->__('Text'),
				'style'     => 'width:100%;height:300px;',
				'required'  => true,
				'config'    => Mage::getSingleton('news/wysiwyg_config')->getConfig()
			));

			if (Mage::registry('news')) {
				$form->setValues(Mage::registry('news')->getData());
			}

			$form->setUseContainer(true);
			$this->setForm($form);
			return parent::_prepareForm();
		}
	}