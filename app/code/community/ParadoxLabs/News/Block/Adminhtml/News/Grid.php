<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_News Magento Plugin
 *	Create and edit simple news entries via the Admin Panel to be displayed on the front-end.
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_News_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
	{

		public function __construct()
		{
			parent::__construct();
			$this->setId('newsGrid');
			$this->setDefaultSort('id');
			$this->setDefaultDir('DESC');
			$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
			$this->setCollection(Mage::getModel('news/news')->getCollection());
			return parent::_prepareCollection();
		}

		protected function _prepareColumns()
		{
			$this->addColumn('id', array(
				'header'    => Mage::helper('news')->__('ID'),
				'align'     => 'right',
				'width'     => '50px',
				'index'     => 'news_id',
				'type'      => 'number',
			));

/*			$this->addColumn('news_date', array(
				'header'    => Mage::helper('news')->__('Date'),
				'align'     => 'left',
				'index'     => 'news_date',
				'type'		=> 'number',
			));*/

			$this->addColumn('news_title', array(
				'header'    => Mage::helper('news')->__('Title'),
				'align'     => 'left',
				'index'     => 'news_title',
			));

			$this->addColumn('news_text', array(
				'header'    => Mage::helper('news')->__('Text'),
				'align'     => 'left',
				'index'     => 'news_text',
			));
			
			return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			return $this->getUrl('*/*/edit', array('id' => $row->getId()));
		}

	}