<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_News Magento Plugin
 *	Create and edit simple news entries via the Admin Panel to be displayed on the front-end.
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_News_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
	{
		public function __construct()
		{
			$this->_controller = 'adminhtml_news';
			$this->_blockGroup = 'news';
			$this->_headerText = Mage::helper('news')->__('Manage News');
			$this->_addButtonLabel = Mage::helper('news')->__('Add News Entry');
			parent::__construct();
		}
	}