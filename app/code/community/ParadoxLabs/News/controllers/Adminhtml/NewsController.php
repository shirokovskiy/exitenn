<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_News Magento Plugin
 *	Create and edit simple news entries via the Admin Panel to be displayed on the front-end.
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	class ParadoxLabs_News_Adminhtml_NewsController extends Mage_Adminhtml_Controller_Action
	{
		public function indexAction()
		{
			$this->loadLayout();
			$this->_setActiveMenu('cms/news');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('News'), Mage::helper('adminhtml')->__('News'));
			$this->renderLayout();
		}

		public function editAction()
		{
			$this->loadLayout();
			$this->_setActiveMenu('cms/news');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('News'), Mage::helper('adminhtml')->__('News'));

			$this->_addContent($this->getLayout()->createBlock('news/adminhtml_news_edit'));
			$this->renderLayout();
		}

		public function newAction()
		{
			$this->editAction();
		}

		public function saveAction()
		{
			if ( $this->getRequest()->getPost() ) {
				try {
					$model = Mage::getModel('news/news')
						->setNewsId($this->getRequest()->getParam('id'))
						->setNewsDate(time())
						->setNewsStore($this->getRequest()->getParam('news_store'))
						->setNewsTitle($this->getRequest()->getParam('news_title'))
						->setNewsText($this->getRequest()->getParam('news_text'))//."\nID:".$this->getRequest()->getParam('news_store')."\n".print_r($this->getRequest()->getPost(),true))
						->save();

					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Entry was successfully saved'));

					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}
			$this->_redirect('*/*/');
		}

		public function deleteAction()
		{
			if( $this->getRequest()->getParam('id') > 0 ) {
				try {
					$model = Mage::getModel('news/news');
					$model->setNewsId($this->getRequest()->getParam('id'))->delete();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Entry was successfully deleted'));
					$this->_redirect('*/*/');
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				}
			}
			$this->_redirect('*/*/');
		}

		protected function _isAllowed()
		{
			return Mage::getSingleton('admin/session')->isAllowed('cms/news');
		}
	}