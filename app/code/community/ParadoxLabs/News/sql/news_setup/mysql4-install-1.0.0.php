<?php
/*
 *	Paradox Labs, Inc.
 *	http://www.paradoxlabs.com
 *	717.431.3330
 *	Twitter: ParadoxLabs
 *
 *	ParadoxLabs_News Magento Plugin
 *	Create and edit simple news entries via the Admin Panel to be displayed on the front-end.
 *
 *	Having a problem with the plugin?
 *	Not sure what something means?
 *	Need custom development?
 *		Give us a call!
 *
 *	Copyright 2010, Paradox Labs, Inc.
 *	Licensed under OSL 3.0 License
 *		http://opensource.org/licenses/osl-3.0.php
 */

	$table = $this->getTable('news');
	
	$this->startSetup()->run("
		drop table if exists {$table};
		create table {$table} (
			news_id int(11) unsigned not null auto_increment,
			news_store int(11) not null,
			news_date int(11) not null,
			news_title varchar(255) not null default '',
			news_text text not null default '',
			PRIMARY KEY(news_id)
		) engine=InnoDB default charset=utf8;
	")->endSetup();
