<?php
/**
 * Cloudzoom Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   QS
 * @package    QS_Cloudzoom
 * @author     Quart-soft Magento Team <magento@quart-soft.com>  
 * @copyright  Copyright (c) 2010 Quart-soft Ltd http://quart-soft.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class QS_Cloudzoom_Model_Source_Position
{
	public static function toOptionArray()
	{
		$list = array(
					"left" => Mage::Helper('cloudzoom')->__('left'),
					"right" => Mage::Helper('cloudzoom')->__('right'),
					"top" => Mage::Helper('cloudzoom')->__('top'),
                    "bottom" => Mage::Helper('cloudzoom')->__('bottom'),
                    "inside" => Mage::Helper('cloudzoom')->__('inside'),
					);
		
		return ($list);
	}
}