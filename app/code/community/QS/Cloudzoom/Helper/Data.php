<?php
/**
 * Cloudzoom Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   QS
 * @package    QS_Cloudzoom
 * @author     Quart-soft Magento Team <magento@quart-soft.com>  
 * @copyright  Copyright (c) 2010 Quart-soft Ltd http://quart-soft.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class QS_Cloudzoom_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getImage($_product, $_image, $_fixsize, $_imgsize) {

        $_originalSizes = getimagesize($_image->getPath());

        switch($_fixsize) {
            case "width":
                    $width = $_imgsize;
                    $height = $_originalSizes[1]/$_originalSizes[0]*$width;
                break;
            case "height":
                    $height = $_imgsize;
                    $width = $_originalSizes[0]/$_originalSizes[1]*$height;
                break;
            case "auto":
                    if ($_originalSizes[0] < $_originalSizes[1]) {
                        $height = $_imgsize;
                        $width = $_originalSizes[0]/$_originalSizes[1]*$height;
                    } else {
                        $width = $_imgsize;
                        $height = $_originalSizes[1]/$_originalSizes[0]*$width;
                    }
                break;
            case "both":
                    $height = $_imgsize;
                    $width = $_imgsize;
                break;
        }
        return Mage::helper('catalog/image')->init($_product,'image',$_image->getFile())->resize($width, $height);
    }

    public function imageToVarien($_product)
    {
		$images = $_product->getMediaGalleryImages();
		if($images->getSize()){
			$image = $images->getFirstItem(); 
			foreach ($images as $_image){
				if($_image->getFile() == $_product->getImage()){
					return $_image;
				}
			}
			return $image;
		} else {
			$images = $_product->getMediaGallery('images');			
			$image = $images[0];
			foreach ($images as $_image){
				if($_image['file'] == $_product->getImage()){
					$image = $_image;
				}
			}			
			$image['url'] = $_product->getMediaConfig()->getMediaUrl($image['file']);
			$image['id'] = isset($image['value_id']) ? $image['value_id'] : null;
			$image['path'] = $_product->getMediaConfig()->getMediaPath($image['file']);			
			return new Varien_Object($image);
		}
    }
}